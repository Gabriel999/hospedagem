<div class="container">
    <div class="row">
        <div class="col-md-9 mx-auto mt-5">
            <table class="table">
                <thead class="black white-text">
                    <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Celular</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>   
                    <?= $table ?>
                </tbody>        
        </div>
    </div>
</div>