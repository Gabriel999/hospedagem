<?php 
include_once 'Pessoa.php';
class  Aluno extends Pessoa{
    /*
    * atributos da classe 
    */
    private $turma; 
  
    /*
    * Construtor da classe 
    */
    public function setTurma($turma){
        $this->turma = $turma;
    }

    /*
    * Metodos de acesso na classe (get ou set)
    */
    public function getTurma(){
        return $this->turma;
    }
}

