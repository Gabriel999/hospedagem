<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teoria extends CI_Controller {
    function __construct(){
        // execultar o construtor da classe pai 
        parent::__construct();
        $this->load->model('TeoriaModel', 'teoria');

    }
  
    public function heranca(){
        $this->teoria->heranca();

    }
    public function helpers(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->teoria->helpers();
        $this->load->view('common/footer');

    }
    public function libraries(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('common/footer');

    }
}
