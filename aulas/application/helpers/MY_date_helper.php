<?php 

function hoje(){
    $d = Date('d/M/Y');
    return $d;
}

function nome_mes($index){
        $mes = array('',
        'Janeiro', 
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro',
    );
    return $mes[$index];
}

function data_atual(){
    $d = Date('d');
    $m = Date('n');
    $y = Date('Y');

    $mes = nome_mes($m);

    return "Guarulhos, $d de $mes de $y.";
}