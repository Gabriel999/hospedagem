<div class="container">
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="text-center">Endereço</h3>
        </div>
        <div class="container">
            <div class="row mx-auto">
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" name="logradouro"  value="<?= set_value('logradouro'); ?>" id="logradouro" placeholder="Avenida, Rua, etc...">
                        <label for="logradouro">Logradouro</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="nome_logradouro" name="nome_logradouro" value="<?= set_value('nome_logradouro'); ?>"  placeholder="Paulista, Faria lima, etc...">
                        <label for="nome_logradouro">Nome Logradouro</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mx-auto">
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" name="numero" value="<?= set_value('numero'); ?>"  id="numero" placeholder="Número">
                        <label for="numero">Número</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="complemento" name="complemento" value="<?= set_value('complemento'); ?>"  placeholder="Complemento">
                        <label for="complemento">Complemento</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row  mx-auto">
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="bairro" name="bairro" value="<?= set_value('bairoo'); ?>" placeholder="Bairro">
                        <label for="bairro">Bairro</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="cep" name="cep"  value="<?= set_value('cep'); ?>" placeholder="C.E.P">
                        <label for="cep">C.E.P</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row  mx-auto">
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="cidade" name="end_cidade"  value="<?= set_value('end_cidade'); ?>" placeholder="Cidade">
                        <label for="cidade">Cidade</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="estado" name="end_estado" value="<?= set_value('end_estado'); ?>"  placeholder="Estado">
                        <label for="estado">Estado</label>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-black btn-md">Enviar</button>
        </form>
    </div>

</div>