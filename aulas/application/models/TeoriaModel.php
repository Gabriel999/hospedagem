<?php
include_once APPPATH.'libraries/teoria/Pessoa.php';
include_once APPPATH.'libraries/teoria/Aluno.php';
class TeoriaModel extends CI_Model{

    public function helpers(){
        echo '<h3>criando helper</h3><br>';
        echo 'carregamento:<br>';
        $this->load->helper('ifsp');
        $a = nome_curso();
        $b = nome_disciplina();
        $c = nome_curto();

        echo "Nos somos do curso $a, estamos cursando a disciplina $b que aparece para nos como $c";

        echo '<h3>incrementando helper existente</h3><br>';
        $this->load->helper('date');
        echo data_atual();
    }


    public function heranca(){
        $p1 = new Pessoa('Gabriel', 17);
        $p2 = new Pessoa('José', 28);
        var_dump($p1); echo '<br>';
        var_dump($p2); echo '<br>';
        echo "<br>";
        echo "Pressão de: " .$p1->getNome(). "=" .$p1->getPressao(). "<br>";
        echo "Pressão de: " .$p2->getNome(). "=" .$p2->getPressao(). "<br>";
        echo "<br>";
        $p1->setIdade(rand(20,40));
        $p2->setIdade(rand(25,50));
        echo "Idade de: " .$p1->getNome(). "=" .$p1->getIdade(). "<br>";
        echo "Idade de: " .$p2->getNome(). "=" .$p2->getIdade(). "<br>";
        echo "<br>";
        echo "Posição de: " .$p1->getNome(). "=" .$p1->andar(). "<br>";
        echo "Posição de: " .$p2->getNome(). "=" .$p2->andar(). "<br>";
        echo "<br>";
        
        $a1 = new Aluno('Gabriel', 17);
        $a1->setTurma('5A');
        var_dump($p1); echo '<br>';

        echo "Pressão de: " .$a1->getNome(). " = " .$a1->getPressao(). "<br>";
        echo "Idade de: " .$a1->getNome(). " = " .$a1->getIdade(). "<br>";
        echo "Posição de: " .$a1->getNome(). " = " .$a1->andar(). "<br>";
        echo "A turma de: " .$a1->getNome(). " = " .$a1->getTurma(). "<br>";


    }
}
