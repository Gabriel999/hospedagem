<?php
 class UserCard {
    /*
    * atributos da classe 
    */
    private $nome;     
    private $sobrenome;     
    private $email;     
    private $celular;     
    private $data_matricula;     
    private $imagem;     

    function __construct(array $data){
        $this->nome = $data['nome'];
        $this->sobrenome = $data['sobrenome'];
        $this->email = $data['email'];
        $this->celular = $data['celular'];
        $this->data_matricula = $data['last_modified'];
        $this->imagem = $data['imagem'];
    }

    public function getHtml() {
        return '<div class="row ">
                    <div class="col-md-12 mt-5 mx-auto">
                        '.$this->card().'
                    </div>
                </div>';
    }

    private function card(){
        return'<div class="card">
                    <div class="row">
                        <img class="card-img-top col-md-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20('.$this->imagem.').jpg" alt="Card image cap">
                        '.$this->card_body().'
                    <div>
                </div>';

    }
    private function card_body(){
        return '<div class="card-body col-md-9">
                    <h4 class="card-title"><a>'.$this->nome.' '.$this->sobrenome.'</a></h4>
                    <p class="card-title">Email: '.$this->email.'</p>
                    <p class="card-title">Celular: '.$this->celular.'</p>
                    <p class="card-title">Dia de cadastrado: '.$this->data_matricula.'</p>
                </div> ';
    }
 }
 