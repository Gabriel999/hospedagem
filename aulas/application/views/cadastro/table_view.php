<div class="container mt-5">
    <div class="row">
        <? $table ?>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-9 mx-auto mt-5">
            <h3>Lista de bairros</h3>
            <table class="table">
                <thead class="black white-text">
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Bairro</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Estado</th>
                    </tr>
                </thead>
                <tbody>
                    <?= $table ?>
                </tbody>
            </table>
        </div>
    </div>
</div>