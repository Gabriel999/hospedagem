<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Controlador responsavel por: 
     * Exibir, explicar a empresa e
     * Cadastrar as duvidas  
     */
    public function __construct(){
        parent::__construct();
        
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('common/footer');
        $this->load->view('whatsapp/botao');

    }
    
	public function index($id = null){
		
        $this->load->view('home/inicio');
        $this->load->view('home/apresentacao');
        $this->load->view('home/importancia');
        $this->load->view('home/parceiros');
        $this->load->view('whatsapp/botao');
        $this->load->view('common/rodape');

		
	}
	
	public function empresa($id = 1){
		
        $this->load->model('EmpresaModel');
        $a = $this->EmpresaModel->compromisso($id);
        $data ['img']= $this->load->view('empresa/imagem', null, true);
        $data ['text']= $this->load->view('empresa/home', $a, true);
        $data ['meio'] = $this->load->view('empresa/meio', $a, true);
        $data ['valores'] = $this->load->view('empresa/valores', $a, true);
        $this->load->view('empresa/layout', $data);
        $this->load->view('whatsapp/botao');
        $this->load->view('common/rodape');
    }

    
	public function servicos($id = null){
		
        /**
         * Dados que vem do banco de daos 
         */
        $this->load->model('ServicoModel', 'servico');
        $cond = $this->servico->condominio($id = 1 );
        $res = $this->servico->residencia($id = 2 );
        $comer = $this->servico->comercio($id = 3 );
        $emp = $this->servico->empresa($id = 4 );
        $igre = $this->servico->igrejas($id = 5 );
        $esco = $this->servico->escolas($id = 6 );
        $hot = $this->servico->hotel($id = 7 );

        /**
         * Fotos para ser exibidas
         */
        $res ['foto'] = base_url('imagens/residencia.jpeg');
        $comer ['foto'] = base_url('imagens/comercio.jpg');
        $cond ['foto'] = base_url('imagens/condominio.jpg');
        $emp ['foto'] = base_url('imagens/empresa.jpg');
        $igre ['foto'] = base_url('imagens/igreja.jpeg');
        $esco ['foto'] = base_url('imagens/escola.jpg');
        $hot ['foto'] = base_url('imagens/hotel.jpg');

        /**
         * carregamentos das views  
         */ 

        $this->load->view('servicos/cabecalho');
        $this->load->view('servicos/direita', $res);
        $this->load->view('pac/botao');
        $this->load->view('servicos/linha');
        $this->load->view('servicos/esquerda', $comer);
        $this->load->view('servicos/direita', $cond);
        $this->load->view('servicos/linha');
        $this->load->view('servicos/esquerda', $emp);
        $this->load->view('servicos/direita', $igre);
        $this->load->view('servicos/linha');
        $this->load->view('servicos/esquerda', $esco);
        $this->load->view('servicos/direita', $hot);
        $this->load->view('common/rodape');
    }
    
    public function contatos(){
        
        $this->load->model('ContatoModel', 'model');
        $this->model->salvar();
        /**
         * Página em aprovação por conta do endereço do cliente
         * 
         * $this->load->view('contato/dados'); 
         * $this->load->view('contato/icones'); 
         * 
         */
        $this->load->view('contato/contato');
        $this->load->view('common/rodape');
    }
}
