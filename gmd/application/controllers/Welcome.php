<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('home/comeco');
		$this->load->view('home/empresa');
		$this->load->view('home/servico1');
		$this->load->view('home/servico');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
	public function empresa()
	{
		
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('empresa/home');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
	public function servico()
	{
		
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('empresa/home');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
	public function portifolio()
	{
		
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('empresa/home');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
	public function contato()
	{
		
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->model('ContatoModel', 'contato');
		$this->contato->salvar();
		$this->load->view('contato/contato');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
}
