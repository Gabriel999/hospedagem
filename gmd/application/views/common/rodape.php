

    
    <!-- s-footer
    ================================================== -->
    <footer class="s-footer">

        <div class="s-footer__main">
            <div class="row">

                <div class="col-two md-four mob-full s-footer__sitelinks">

                    <h4>Quick Links</h4>

                    <ul class="s-footer__linklist">
                        <li><a href="#0">Home</a></li>
                        <li><a href="#0">About</a></li>
                        <li><a href="#0">Serviços</a></li>
                        <li><a href="#0">Portifolio </a></li>
                        <li><a href="#0">Contatos</a></li>
                    </ul>

                </div> <!-- end s-footer__sitelinks -->

                <div class="col-two md-four mob-full s-footer__archives">

                    <h4>Serviços</h4>

                    <ul class="s-footer__linklist">
                        <li><a href="#0">Manutenção de sites</a></li>
                        <li><a href="#0">Criação de sistemas web</a></li>
                        <li><a href="#0">SEO (Otimização de sites)</a></li>
                        <li><a href="#0">Criação de sites esticos e dinâmicos</a></li>
                        <li><a href="#0">Reformulação de sites</a></li>
                    </ul>

                </div> <!-- end s-footer__archives -->

                <div class="col-two md-four mob-full s-footer__social">

                    <h4>Redes Social</h4>

                    <ul class="s-footer__linklist">
                        <li><a href="#0">Facebook</a></li>
                        <li><a href="#0">Instagram</a></li>
                        <li><a href="#0">Whatsapp</a></li>
                        
                    </ul>

                </div> <!-- end s-footer__social -->

                <div class="col-five md-full end s-footer__subscribe">

                    <h4>GMDesenvolvimento</h4>

                    <p>Somos uma empresa de desenvolvimento de websites, estamos localizados em Guarulhos. 
                        Nosso principal intuito é desenvolver plataformas de uma forma que faça sua empresa crescer 
                        cada vez mais no mercado!
                    </p>
                    

                </div> 
            </div>
        </div> 

        <div class="s-footer__bottom">
            <div class="row">
                <div class="col-full">
                    <div class="s-footer__copyright">
                        <span>© Copyright GMDesenvolvimento 2019</span>
                    </div>

                    <div class="go-top">
                        <a class="smoothscroll" title="Back to Top" href="#top"></a>
                    </div>
                    
                </div>
            </div>
        </div> 

    </footer>  