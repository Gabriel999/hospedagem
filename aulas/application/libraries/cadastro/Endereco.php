<?php

class Endereco {

    private $db;

    function __construct(){
        $ci = & get_instance();
        $this->db = $ci->db;
    }

    public function gravar($data, $id_usuario){
        $data['id_dados_pessoais'] = $id_usuario;
        $this->db->insert('endereco', $data);
        return $this->db->insert_id();
    } 
    public function listaBairros(){
        $sql = "SELECT nome, bairro, end_cidade, end_estado FROM  dados_pessoais, endereco WHERE dados_pessoais.id = endereco.id_dados_pessoais";
        $rs = $this->db->query($sql);
        return $rs->result();
    }
}

