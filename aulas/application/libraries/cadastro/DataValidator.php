<?php 
class DataValidator {
    private $form_validation;

    function __construct() {
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }

    public function getData(){
        $data ['nome'] = $_POST['nome'];
        $data ['sobrenome']= $_POST['sobrenome'];
        $data ['nascimento']= $_POST['nascimento'];
        $data ['cidade']= $_POST['cidade'];
        $data ['estado']= $_POST['estado'];
        return $data;
    }
    public function validate(){
        $this->form_validation->set_rules('nome','nome','required|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('sobrenome','sobrenome','required|max_length[100]');
        $this->form_validation->set_rules('nascimento','nascimento','required|min_length[6]|max_length[10]');
        $this->form_validation->set_rules('cidade','cidade','required|max_length[28]');
        $this->form_validation->set_rules('estado','estado','required|min_length[2]|max_length[20]');

    }
}

