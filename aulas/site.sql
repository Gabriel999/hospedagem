-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:5050
-- Generation Time: 26-Abr-2019 às 21:00
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados_pessoais`
--

CREATE TABLE `dados_pessoais` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `sobrenome` varchar(70) NOT NULL,
  `nascimento` varchar(12) NOT NULL,
  `cidade` varchar(128) NOT NULL,
  `estado` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados_pessoais`
--

INSERT INTO `dados_pessoais` (`id`, `nome`, `sobrenome`, `nascimento`, `cidade`, `estado`) VALUES
(1, 'Gabriel', 'Moraes', '19/04/2001', 'Guarulhos', 'SP');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` int(11) NOT NULL,
  `id_dados_pessoais` int(11) NOT NULL,
  `logradouro` varchar(30) NOT NULL,
  `nome_logradouro` varchar(128) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(60) NOT NULL,
  `bairro` varchar(128) NOT NULL,
  `end_cidade` varchar(128) NOT NULL,
  `end_estado` varchar(128) NOT NULL,
  `cep` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id`, `id_dados_pessoais`, `logradouro`, `nome_logradouro`, `numero`, `complemento`, `bairro`, `end_cidade`, `end_estado`, `cep`) VALUES
(1, 1, 'Estrada dos morros', 'Estrada dos morros', 578, 'bloco 04 apartamento 54 ', 'cocaia', 'Guarulhos', 'SP', '07131-190');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subtitulo` varchar(500) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `titulo`, `subtitulo`, `last_modified`) VALUES
(1, 'Loginha da esquina', 'CORRE QUE TEM PROMOÇÃO', '2019-03-23 04:20:50'),
(2, 'Sofá por apenas', '100 R$', '2019-03-23 04:20:50'),
(3, 'cama por apenas', '10 R$', '2019-03-23 04:20:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `jumbo`
--

CREATE TABLE `jumbo` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subtitulo` varchar(500) NOT NULL,
  `btn` varchar(20) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `jumbo`
--

INSERT INTO `jumbo` (`id`, `titulo`, `subtitulo`, `btn`, `last_modified`) VALUES
(1, 'Loja da esquina', 'PROMOÇÃO\r\n', 'SAIBA MAIS', '2019-03-23 04:19:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `senha` varchar(512) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `imagem` tinyint(3) UNSIGNED NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `sobrenome`, `email`, `senha`, `celular`, `imagem`, `last_modified`) VALUES
(2, 'Gabiga', 'linda', 'gabigagata@gmail.com', '1029', '994010204', 7, '2019-03-26 16:41:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jumbo`
--
ALTER TABLE `jumbo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jumbo`
--
ALTER TABLE `jumbo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
