<?php

class DadosPessoais 
{
    private $db;
    function __construct(){
        // classe criada pela gente
        $ci = & get_instance();
        $this->db = $ci->db;
    }

    public function gravar($data){
        $this->db->insert('dados_pessoais', $data);
        return $this->db->insert_id();
    } 
}

