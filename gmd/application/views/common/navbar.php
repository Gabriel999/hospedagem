 <!-- pageheader
    ================================================== -->
 <div class="s-pageheader">

     <header class="header">
         <div class="header__content row">

             <div class="header__logo">
                 <a class="logo" href="index.html">
                     <img src="<?= base_url('images/logo.png') ?>" alt="Homepage">
                 </a>
             </div> <!-- end header__logo -->

             <ul class="header__social">
                 <li>
                     <a href="#0"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                 </li>

                 <li>
                     <a href="#0"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                 </li>
                 <li>
                     <a href="#0"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                 </li>

             </ul> <!-- end header__social -->
             <!-- end header__social -->



             <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>

             <nav class="header__nav-wrap">

                 <h2 class="header__nav-heading h6">GMDesenvolvimento</h2>

                 <ul class="header__nav">
                     <li class=""><a href="<?= base_url() ?>" title="">Home</a></li>
                     <li><a href="<?= base_url('index.php/welcome/empresa') ?>" title="">Quem somos?</a></li>
                     <li><a href="<?= base_url('index.php/welcome/servico') ?>" title="">Serviços</a></li>
                     <li><a href="<?= base_url('index.php/welcome/portifolio') ?>" title="">Portifolio</a></li>
                     <li><a href="<?= base_url('index.php/welcome/contato') ?>" title="">Contatos</a></li>
                 </ul> <!-- end header__nav -->

                 <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

             </nav> <!-- end header__nav-wrap -->

         </div> <!-- header-content -->

     </header> <!-- header -->

 </div>