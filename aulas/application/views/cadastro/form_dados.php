<div class="container">
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="card mt-3">
        <form method="POST">
            <div class="card-header">
                <h3 class="text-center">Dados pessoais</h3>
            </div>
            <div class="row col-md-12 mx-auto">
                <div class="md-form col-md-5 ml-md-5">
                    <i class="fas fa-user prefix"></i>
                    <input type="text" id="nome" name="nome" value="<?= set_value('nome'); ?>" class="form-control">
                    <label class="ml-md-5" for="nome">Nome</label>
                </div>
                <div class="md-form col-md-5 ml-md-5">
                    <i class="fas fa-user prefix"></i>
                    <input type="text" id="sobrenome" name="sobrenome" value="<?= set_value('sobrenome'); ?>"  class="form-control validate">
                    <label class="ml-md-5" for="sobrenome" data-error="wrong" data-success="right">Sobrenome</label>
                </div>
            </div>
            <div class="row col-md-12 mx-auto ">
                <div class="md-form col-md-3 ml-md-5">
                    <i class="fas fa-calendar prefix"></i>
                    <input type="text" id="nascimento" name="nascimento"  value="<?= set_value('nascimento'); ?>" class="form-control">
                    <label class="ml-md-5" for="nascimento">Nascimento</label>
                </div>
                <div class="md-form col-md-5 ml-md-5">
                    <i class="fas fa-city prefix"></i>
                    <input type="text" id="cidade" name="cidade" value="<?= set_value('cidade'); ?>"  class="form-control validate">
                    <label class="ml-md-5" for="cidade" data-error="wrong" data-success="right">Cidade</label>
                </div>
                <div class="md-form col-md-2 ml-md-5">
                    <i class="fas fa-city prefix"></i>
                    <input type="text" id="estado" name="estado"  value="<?= set_value('estado'); ?>" class="form-control validate">
                    <label class="ml-md-5" for="estado" data-error="wrong" data-success="right">Estado</label>
                </div>
            </div>
        </div>
</div>