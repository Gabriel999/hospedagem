    </div> <!-- end row -->
    <div class="row top">

        <div class="col-eight md-six tab-full popular">
            <h3>Serviços oferecidos</h3>

            <div class="block-1-2 block-m-full popular__posts">
                <article class="col-block popular__post">
                    <a href="#0" class="popular__thumb">
                        <i class="fas fa-wrench" style="font-size:30pt"></i>
                    </a>
                    <h5><a href="#0">Manutenção de sites</a></h5>

                </article>
                <article class="col-block popular__post">
                    <a href="#0" class="popular__thumb">
                        <i class="fas fa-code" style="font-size:30pt"></i>
                    </a>
                    <h5><a href="#0">Criação de sites esticos e dinâmicos</a></h5>

                </article>
                <article class="col-block popular__post">
                    <a href="#0" class="popular__thumb">
                        <i class="fas fa-shopping-cart" style="font-size:30pt"></i>
                    </a>
                    <h5><a href="#0">Criação de sistemas web</a></h5>

                </article>
                <article class="col-block popular__post">
                    <a href="#0" class="popular__thumb">
                        <i class="fas fa-cogs" style="font-size:30pt"></i>

                    </a>
                    <h5><a href="#0">Reformulação de sites</a></h5>

                </article>
                <article class="col-block popular__post">
                    <a href="#0" class="popular__thumb">
                        <i class="fas fa-chart-line" style="font-size:30pt"></i>
                    </a>
                    <h5><a href="#0">SEO (Otimização de sites)</a></h5>

                </article>
                <a>
                    <button class="col-blocks" type="sumit" style="text-align: center">saiba mais sobre os serviços</button>
                </a>


            </div> <!-- end popular_posts -->
        </div> <!-- end popular -->
    </div> <!-- end row -->
    </section> <!-- end s-extra -->