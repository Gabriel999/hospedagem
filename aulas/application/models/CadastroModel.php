<?php
defined('BASEPATH') or exit('No direct script access allowed');
include APPPATH . 'libraries/cadastro/DataValidator.php';
include APPPATH . 'libraries/cadastro/AddressValidator.php';
include APPPATH . 'libraries/cadastro/DadosPessoais.php';
include APPPATH . 'libraries/cadastro/Endereco.php';

class CadastroModel extends CI_Model
{

    public function salvar(){
        if (sizeof($_POST) == 0) return;

        $data = new DataValidator();
        $addr = new AddressValidator();
        
        $data->validate();
        $addr->validate();

        if($this->form_validation->run()){
            $dados = new DadosPessoais();
            $v = $data->getData();
            $id = $dados->gravar($v);

            $end = new Endereco();
            $u = $addr->getData();
            $end->gravar($u, $id);
        } else {
        }
    }

    public function listaBairros(){
        // obter os dados do banco 
        $end = new Endereco();
        $bairros = $end->listaBairros();
        $html = '';
        // definir o ccabeçalho da tabela
        // gerar a tabela
        // retornar o html da tabela
        foreach ($bairros as $row) {
            $html .= "<tr>";
            $html .= "<td>$row->nome</td>";
            $html .= "<td>$row->bairro</td>";
            $html .= "<td>$row->end_cidade</td>";
            $html .= "<td>$row->end_estado</td>";
            $html .= "</tr>";

        }
        return $html;
    }
}
