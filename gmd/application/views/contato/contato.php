<!-- s-content
    ================================================== -->
    <section class="s-content s-content--narrow">

        <div class="row">

            <div class="s-content__header col-full">
                <h1 class="s-content__header-title">
                    Contato
                </h1>
            </div> <!-- end s-content__header -->
    

            <div class="col-full s-content__main">

                
                <div class="row">
                    <div class="col-six tab-full">
                        <h3>Onde nos encontrar</h3>

                        <p>
                        Estrada dos morros, 578<br>
                        Guarulhos, SP<br>
                        07131-190 BR
                        </p>

                    </div>

                    <div class="col-six tab-full">
                        <h3>Informações de contato</h3>

                        <p>gmdwebservice@gmail.com<br>
                        Celular: (11) 99394-6129
                        </p>

                    </div>
                </div> <!-- end row -->

                <h3>Diga olá.</h3>

                <form name="cForm" id="cForm" method="post">
                    <fieldset>

                        <div class="form-field">
                            <input name="name" type="text" id="cName" class="full-width" placeholder="Nome complero" value="">
                        </div>

                        <div class="form-field">
                            <input name="email" type="text" id="cEmail" class="full-width" placeholder="Email" value="">
                        </div>

                        <div class="form-field">
                            <input name="website" type="text" id="cWebsite" class="full-width" placeholder="Website"  value="">
                        </div>

                        <div class="message form-field">
                        <textarea name="mensagem" id="cMessage" class="full-width" placeholder="Mensagem" ></textarea>
                        </div>

                        <button type="submit" class="submit btn btn--primary full-width">enviar</button>

                    </fieldset>
                </form> <!-- end form -->


            </div> <!-- end s-content__main -->

        </div> <!-- end row -->

    </section> <!-- s-content -->


    