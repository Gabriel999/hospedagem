<?php 
class AddressValidator {

    private $form_validation;

    function __construct() {
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }

    public function getData(){
        $data ['logradouro'] = $_POST['logradouro'];
        $data ['nome_logradouro']= $_POST['nome_logradouro'];
        $data ['numero']= $_POST['numero'];
        $data ['complemento']= $_POST['complemento'];
        $data ['bairro']= $_POST['bairro'];
        $data ['end_cidade']= $_POST['end_cidade'];
        $data ['end_estado']= $_POST['end_estado'];
        $data ['cep']= $_POST['cep'];
        return $data;
    }
    public function validate(){
        $this->form_validation->set_rules('logradouro','logradouro','required');
        $this->form_validation->set_rules('nome_logradouro','nome_logradouro','required');
        $this->form_validation->set_rules('numero','numero','required');
        $this->form_validation->set_rules('complemento','complemento','');
        $this->form_validation->set_rules('bairro','bairro','required');
        $this->form_validation->set_rules('end_cidade','cidade','required');
        $this->form_validation->set_rules('end_estado','estado','required');
        $this->form_validation->set_rules('cep','cep','required');
       
    }
}
