   

        <div class="pageheader-content row">
            <div class="col-full">

                <div class="featured">

                    <div class="featured__column featured__column--big">
                        <div class="entry" style="background-image:url('images/thumbs/featured/11.jpg');">
                            
                            <div class="entry__content">

                                <h1><a href="#0" title="">Soluções inovadoras para seus négocios !</a></h1>

                                
                            </div> <!-- end entry__content -->
                            
                        </div> <!-- end entry -->
                    </div> <!-- end featured__big -->

                    <div class="featured__column featured__column--small">

                        <div class="entry" style="background-image:url('images/thumbs/featured/scrum.jpg');">
                            
                            <div class="entry__content">

                                <h1><a href="#0" title="">Trabalhamos com uma metodologia ágil, para poder te entregar o mais rápido possivel</a></h1>

                                
                            </div> <!-- end entry__content -->
                          
                        </div> <!-- end entry -->

                        <div class="entry" style="background-image:url('images/thumbs/featured/responsivo.png');">

                            <div class="entry__content">

                                <h1><a href="#0" title="">Sites responsivos e de qualidade, venha, não perca tempo.</a></h1>


                            </div> <!-- end entry__content -->

                        </div> <!-- end entry -->

                    </div> <!-- end featured__small -->
                </div> <!-- end featured -->

            </div> <!-- end col-full -->
        </div> <!-- end pageheader-content row -->
        <br>

    </section> <!-- end s-pageheader -->
