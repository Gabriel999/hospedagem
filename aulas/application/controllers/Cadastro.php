<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller{

    public function index(){
        $this->load->model('CadastroModel','model');
        $this->model->salvar();
        $html = $this->load->view('cadastro/form_dados', null, true);
        $html .= $this->load->view('cadastro/form_endereco', null, true);
        $this->show($html);
    }
    public function bairros(){
        $this->load->model('CadastroModel','model');
        $data['table'] = $this->model->listaBairros();
        $html = $this->load->view('cadastro/table_view', $data, true);
        $this->show($html);
    }
    private function show($content)
    {
        $html = $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        $html .= $content;
        $html .= $this->load->view('common/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    
    }
    

}
