<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;
if(ENVIRONMENT != 'development'){
	
	$usuario = 'gu1700928';
	$senha = 'tN0W5Vgyvl2I4xReOWH4iAwJzngE7iw4lC13aB6l8cE=';
	$banco = 'gu1700928';
	
}else{
	$usuario = 'root';
	$senha = '';
	$banco = 'site';
}

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => $usuario,
	'password' => $senha,
	'database' => $banco,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
