<?php 

class  Pessoa{
    /*
    * atributos da classe 
    */
    private $nome;     
    private $idade;     
    private $pressao;   
    private $x = 0;   
    private $y = 0;   
  
    /*
    * Construtor da classe 
    */
    function __construct($nome, $idade){
        $this->nome = $nome;
        $this->idade = $idade;
    }
  
    /*
    * Metodos de acesso na classe (get ou set)
    */
    public function getNome() {
        return $this->nome;

    }

    
    public function getIdade() {
        return $this->idade;
        
    }
    
    public function setIdade($idade) {
        $this->idade = $idade;
    }
    public function getPressao() {
        $s = rand(0, 30);
        $d = rand(0, 20);
        return "$s x $d";
    }
        
    public function andar(){
        $this->x = rand(-30, 30);
        $this->y = rand(-30, 30);
        return "(".$this->x. "," .$this->y.")";
    }
}

