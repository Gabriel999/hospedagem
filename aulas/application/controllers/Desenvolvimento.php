<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class desenvolvimento extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('home/home1');
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
	
	public function precos($id = 1)
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('Model');
        $a = $this->Model->getPreco($id);
        $this->load->view('mercado/home', $a);
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}

	public function jumbo($id = 1)
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');
		$this->load->view('jumbo/carrosel');
		$this->load->model('Model');
        $info = $this->Model->getJumbo($id);
        $a ['jumbo'] = $this->load->view('jumbo/jumbo', $info, true);
        $this->load->view('jumbo/layout', $a);
		$this->load->view('common/rodape');
		$this->load->view('common/footer');
	}
    
}
