<!-- Jumbotron -->
<div class="jumbotron p-0">

  <!-- Card image -->
  <div class="view overlay rounded-top">
    <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(5).jpg" class="img-fluid" alt="Sample image">
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body text-center mb-3">

    <!-- Title -->
    <h3 class="card-title h3 my-4"><strong><?= $titulo ?></strong></h3>
    <!-- Text -->
    <p class="card-text py-2"><?= $subtitulo ?></p>
    <!-- Button -->
    <a href="#" class="btn morpheus-den-gradient btn-rounded mb-4"><?= $btn ?></a>

  </div>

</div>
<!-- Jumbotron -->