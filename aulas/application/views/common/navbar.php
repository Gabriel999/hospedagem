<nav class="mb-1 navbar navbar-expand-lg navbar-dark black">
  <a class="navbar-brand" href="<?= base_url(); ?>">DWA</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url(); ?>">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Mercado
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="<?= base_url('Desenvolvimento/precos/1') ?>">Mercado</a>
          <a class="dropdown-item" href="<?= base_url('Desenvolvimento/precos/2') ?>">Pão</a>
          <a class="dropdown-item" href="<?= base_url('Desenvolvimento/precos/3') ?>">Suco</a>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Desenvolvimento/jumbo'); ?>">Jumbo
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Usuario'); ?>">Lista de cadastrados  
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Usuario/lista'); ?>">Detalhes dos usuarios  
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Usuario/criar'); ?>">Cadastre-se
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Cadastro'); ?>">Cadastro complexos 
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?= base_url('Cadastro/bairros'); ?>">Lista Bairros 
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="teoria" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Teoria helpers
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="teoria">
          <a class="dropdown-item" href="<?= base_url('Teoria/heranca') ?>">Hearança</a>
          <a class="dropdown-item" href="<?= base_url('Teoria/helpers') ?>">Helpers</a>
          <a class="dropdown-item" href="<?= base_url('Teoria/libraries') ?>">Libraries</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light">
          <i class="fab fa-facebook-f"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light">
          <i class="fab fa-instagram"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light">
          <i class="fab fa-twitter"></i>
        </a>
      </li>
    
    </ul>
  </div>
</nav>
<!--/.Navbar -->