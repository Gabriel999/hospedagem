<section class="s-extra">
    <div class="col-four md-six tab-full about">
        <h3>Quem somos ?</h3>

        <p>
            Somos uma empresa de desenvolvimento de websites, estamos localizados em Guarulhos.
            Nosso principal intuito é desenvolver plataformas de uma forma que faça sua empresa crescer cada vez
            mais no mercado!
        </p>

        <ul class="about__social">
            <li>
                <a href="#0"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
            </li>

            <li>
                <a href="#0"><i class="fab fa-instagram" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="#0"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
            </li>

        </ul> <!-- end header__social -->
    </div> <!-- end about -->